{
	"MenuQuiosque": [{
		"item": {
			"name": "Reserva/Cotação de Voo",
			"tag": "ReservaCotacaoTablet",
			"name_lang": {
				"br": "Reserva/Cotação de Voo",
				"de": "Flug reservieren",
				"gb": "Flight Booking",
				"es": "Reserva de vuelo",
				"fr": "Réservation de vol",
				"it": "Prenotazione/Centro del volo"
			},
			"service_url": "",
			"api_callback": "",
			"icon": "imgs/icons/FLIGHT2.png",
			"visible": true,
			"lightbox": false,
			"subitens": [{
				"item": {
					"name": "Compra Pagante",
					"tag": "CompraPaganteTablet",
					"name_lang": {
						"br": "Compra Pagante",
						"de": "Zahlend erwerben",
						"gb": "Purchase Payee",
						"es": "Compra con pago",
						"fr": "Achat payant",
						"it": "Acquisto a pagamento"
					},
					"service_url": "http://book.latam.com/TAM/dyn/air/search?LANGUAGE=BR&SITE=JJBKJJBK&WDS_MARKET=BR&WDS_ANTECEDENCE_OVERRIDE_VALUE=D3&WDS_ANTECEDENCE_OVERRIDE_IATAS=ALL",
					"icon": "imgs/icons/DOCUMENTS.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false,
					"parameters": [{
						"parameter": {
							"name": "LANGUAGE",
							"value": "[LANGUAGE]",
							"format": "",
							"name_lang": {
								"br": "",
								"de": "",
								"gb": "",
								"es": "",
								"fr": "",
								"it": ""
							}
						}
					}]
				}
			}, {
				"item": {
					"name": "Compra com Pontos",
					"tag": "CompraComPontosTablet",
					"name_lang": {
						"br": "Compra com Pontos",
						"de": "Mit Punkten erwerben",
						"gb": "Purchase Air Miles",
						"es": "Compra con puntos",
						"fr": "Achat avec les points",
						"it": "Acquisto con punti"
					},

					"service_url": "http://book.latam.com/TAM/dyn/air/redemption/search?LANGUAGE=BR&SITE=JJRDJJRD&WDS_MARKET=BR&WDS_ANTECEDENCE_OVERRIDE_VALUE=D3&WDS_ANTECEDENCE_OVERRIDE_IATAS=ALL",
					"icon": "imgs/icons/DOCUMENTS2.png",
					"api_callback": "",
					"visible": true,	
					"lightbox": false,
					"parameters": [{
						"parameter": {
							"name": "LANGUAGE",
							"value": "[LANGUAGE]",
							"format": "",
							"name_lang": {
								"br": "",
								"de": "",
								"gb": "",
								"es": "",
								"fr": "",
								"it": ""
							}
						}
					}]
				}
			}]
		}
	}, {
		"item": {
			"name": "Antecipação e Postergação",
			"tag": "AntecipacaoPostegacaoTablet",
			"name_lang": {
				"br": "Antecipação e Postergação",
				"de": "Vorverlegung und späteres Boarding",
				"gb": "Anticipate and Postpone",
				"es": "Adelanto y postergación",
				"fr": "Anticipation et report",
				"it": "Anticipo e posticipo"
			},
			"service_url": "",
			"icon": "imgs/icons/FLIGHT.png",
			"api_callback": "",
			"visible": true,
			"lightbox": false,
			"subitens": [{
				"item": {
					"name": "Antecipação de embarque",
					"tag": "AntecipacaoEmbarqueTablet",
					"name_lang": {
						"br": "Antecipação de embarque",
						"de": "Vorverlegung des Boardings",
						"gb": "Anticipate boarding",
						"es": "Adelanto de embarque",
						"fr": "Embarquement anticipé",
						"it": "Anticipo dell'imbarco"
					},
					"service_url": "https://www.latam.com/flight-anticipation/",
					"icon": "imgs/icons/DOCUMENTS.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Postergação de embarque",
					"tag": "PostergacaoEmbarqueTablet",
					"name_lang": {
						"br": "Postergação de embarque",
						"de": "Späteres Boarding",
						"gb": "Postpone boarding",
						"es": "Postergación de embarque",
						"fr": "Report d'embarquement",
						"it": "Posticipo dell'imbarco"
					},
					"service_url": "https://www.latam.com/pt_br/informacao-para-sua-viagem/postergacao-de-embarque/",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/informacao-para-sua-viagem/postergacao-de-embarque/",
						"de": "https://www.latam.com/de_de/",
						"gb": "https://www.latam.com/en_un/",
						"es": "https://www.latam.com/es_us/",
						"fr": "https://www.latam.com/fr_fr/",
						"it": "https://www.latam.com/it_it/"
					},
					"icon": "imgs/icons/DOCUMENTS2.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}]
		}
	}, {
		"item": {
			"name": "Remarcação e Alteração",
			"tag": "RemarcacaoAlteracaoTablet",
			"name_lang": {
				"br": "Remarcação e Alteração",
				"de": "Verlegung und Veränderungen",
				"gb": "Rebooking and Change",
				"es": "Remarcación y cambio",
				"fr": "Modification",
				"it": "Riprogrammazione e modifica"
			},
			"service_url": "https://www.latam.com/b2c/jsp/latam/entryPoint/entryPointAmadeus.jsp?embedded=false&parametro=revenueRebooking",
			"icon": "imgs/icons/FLIGHT2.png",
			"api_callback": "",
			"visible": true,
			"lightbox": false,
			"msgLightbox": {
				"br": "Remarque ou mude sua passagem",
				"de": "Verlegen oder ändern Sie Ihre Reise",
				"gb": "Reschedule or change your airfare",
				"es": "Reprograme o cambie su pasaje.",
				"fr": "Réservez à nouveau ou modifiez votre billet",
				"it": "Riprogrammi o modifichi il suo biglietto"
			},
			"parameters": [{
				"parameter": {
					"name": "LANGUAGE",
					"value": "[LANGUAGE]",
					"format": "",
					"name_lang": {
						"br": "",
						"de": "",
						"gb": "",
						"es": "",
						"fr": "",
						"it": ""
					}
				}
			}, {
				"parameter": {
					"name": "DIRECT_RETRIEVE_LASTNAME",
					"name_lang": {
						"br": "Sobrenome",
						"de": "Nachname",
						"gb": "Last Name",
						"es": "Apellido",
						"fr": "Nom de famille",
						"it": "Cognome"
					}
				}
			}, {
				"parameter": {
					"name": "REC_LOC",
					"name_lang": {
						"br": "Código da reserva",
						"de": "Reservierungscode",
						"gb": "Booking code",
						"es": "Código de reserva",
						"fr": "Code de réservation",

						"it": "Codice di prenotazione"
					}
				}
			}]
		}
	}, {
		"item": {
			"name": "Check-in",
			"tag": "Checkintablet",
			"name_lang": {
				"br": "Check-in",
				"de": "Check-in",
				"gb": "Check-in",
				"es": "Check-in",
				"fr": "Enregistrement",
				"it": "Check-in"
			},
			"service_url": "",
			"api_callback": "",
			"icon": "imgs/icons/CHECKIN_NOTEBOOK.png",
			"visible": true,
			"lightbox": false,
			"subitens": [{
				"item": {
					"name": "Faça seu check-in",
					"tag": "CheckinTabletDo",
					"name_lang": {
						"br": "Faça seu check-in",
						"de": "Check-in durchführen",
						"gb": "Check-in",
						"es": "Haga su check-in",
						"fr": "Effectuez votre enregistrement",
						"it": "Effettuare il check-in"
					},
					"service_url": "https://checkin.si.amadeus.net/static/PRD/JJ/#/checkindirect?",
					"icon": "imgs/icons/MOBILE-CHECKIN.png",
					"api_callback": "",
					"visible": true,
					"lightbox": true,
					"msgLightbox": {
						"br": "Antecipe o check-in e economize tempo no dia da viagem",
						"de": "Führen Sie den Check-in im Voraus durch und verlieren Sie keine Zeit am Reisetag",
						"gb": "Anticipate your check-in and save time on your travel day",
						"es": "Haga su check-in con antelación y ahorre tiempo el día del viaje",
						"fr": "Effectuez votre enregistrement à l’avance et gagnez du temps le jour où vous voyagez",
						"it": "Anticipi il check-in per risparmiare tempo nel giorno del viaggio"
					},
					"parameters": [{
						"parameter": {
							"name": "ln",
							"value": "[LANGUAGE]",
							"format": "",
							"name_lang": {
								"br": "",
								"de": "",
								"gb": "",
								"es": "",
								"fr": "",
								"it": ""
							}
						}
					}, {
						"parameter": {
							"name": "IIdentification",
							"name_lang": {
								"br": "Código da reserva ou e-ticket",
								"de": "Reservierungscode oder e-Ticket",
								"gb": "Booking code or e-ticket",
								"es": "Código de reserva o ticket electrónico",
								"fr": "Code de réservation ou billet électronique",
								"it": "Codice di prenotazione o e-ticket"
							}
						}
					}, {
						"parameter": {
							"name": "IFormOfIdentification",
							"value": "[IFormOfIdentification]",
							"name_lang": {
								"br": "",
								"de": "",
								"gb": "",
								"es": "",
								"fr": "",
								"it": ""
							}
						}
					}, {
						"parameter": {
							"name": "IDepartureDate",
							"value": "",
							"format": "date",
							"name_lang": {
								"br": "Data de Partida",
								"de": "Versanddatum",
								"gb": "Departure date",
								"es": "Fecha de salida",
								"fr": "Date de départ",
								"it": "Data di partenza"
							}
						}
					}]
				}
			}, {
				"item": {
					"name": "Cancele o check-in",
					"tag": "CheckintabletCancel",
					"name_lang": {
						"br": "Cancele o check-in",
						"de": "Check-in stornieren",
						"gb": "Cancel your check-in",
						"es": "Cancele el check-in",
						"fr": "Annulez votre enregistrement",
						"it": "Annullare il check-in"
					},
					"service_url": "https://checkin.si.amadeus.net/static/PRD/JJ/#/checkindirect?",
					"icon": "imgs/icons/MOBILE-CHECKIN.png",
					"api_callback": "",
					"visible": true,
					"lightbox": true,
					"msgLightbox": {
						"br": "",
						"de": "",
						"gb": "",
						"es": "",
						"fr": "",
						"it": ""
					},
					"parameters": [{
						"parameter": {
							"name": "ln",
							"value": "[LANGUAGE]",
							"format": "",
							"name_lang": {
								"br": "",
								"de": "",
								"gb": "",
								"es": "",
								"fr": "",
								"it": ""
							}
						}
					}, {
						"parameter": {
							"name": "IIdentification",
							"name_lang": {
								"br": "Código da reserva ou e-ticket",
								"de": "Reservierungscode oder e-Ticket",
								"gb": "Booking code or e-ticket",
								"es": "Código de reserva o ticket electrónico",
								"fr": "Code de réservation ou billet électronique",
								"it": "Codice di prenotazione o e-ticket"
							}
						}
					}, {
						"parameter": {
							"name": "IFormOfIdentification",
							"value": "[IFormOfIdentification]",
							"name_lang": {
								"br": "",
								"de": "",
								"gb": "",
								"es": "",
								"fr": "",
								"it": ""
							}
						}
					}, {
						"parameter": {
							"name": "IDepartureDate",
							"value": "",
							"format": "date",
							"name_lang": {
								"br": "Data de Partida",
								"de": "Versanddatum",
								"gb": "Departure date",
								"es": "Fecha de salida",
								"fr": "Date de départ",
								"it": "Data di partenza"
							}
						}
					}]
				}
			}]
		}
	}, {
		"item": {
			"name": "Consulte sua reserva",
			"tag": "CosultesuareservaTablet",
			"name_lang": {
				"br": "Consulte sua reserva",
				"de": "Buchung abfragen",
				"gb": "Consult your Booking",
				"es": "Consulte su reserva",
				"fr": "Consultez votre réservation",
				"it": "Consulti la prenotazione"
			},
			"service_url": "",
			"api_callback": "",
			"icon": "imgs/icons/VERIFY2.png",
			"visible": true,
			"lightbox": false,
			"subitens": [{
				"item": {
					"name": "Informações sobre sua reserva",
					"tag": "InformaSobreSuaReservaTablet",
					"name_lang": {
						"br": "Informações sobre sua reserva",
						"de": "Informationen zu Ihrer Reservierung",
						"gb": "Information about your booking",
						"es": "Información sobre su reserva",
						"fr": "Informations sur votre réservation",
						"it": "Informazioni sulla prenotazione"
					},

					"service_url": "https://book.latam.com/TAM/dyn/air/servicing/retrievePNR?ACTION=MODIFY&DIRECT_RETRIEVE=TRUE&SITE=JJBKJJBK&WDS_MARKET=BR",
					"icon": "imgs/icons/VERIFY2.png",
					"api_callback": "",
					"visible": true,
					"lightbox": true,
					"msgLightbox": {
						"br": "Informações sobre sua reserva",
						"de": "Informationen zu Ihrer Reservierung",
						"gb": "Information about your booking",
						"es": "Información sobre su reserva",
						"fr": "Informations sur votre réservation",
						"it": "Informazioni sulla prenotazione"
					},
					"parameters": [{
						"parameter": {
							"name": "DIRECT_RETRIEVE_LASTNAME",
							"value": "",
							"format": "",
							"name_lang": {
								"br": "Sobrenome",
								"de": "Nachname",
								"gb": "Last Name",
								"es": "Apellido",
								"fr": "Nom de famille",
								"it": "Cognome"
							}
						}
					}, {
						"parameter": {
							"name": "REC_LOC",
							"value": "",
							"format": "",
							"name_lang": {






								"br": "Código da reserva",
								"de": "Reservierungscode",
								"gb": "Booking code",
								"es": "Código de reserva",
								"fr": "Code de réservation",
								"it": "Codice di prenotazione"
							}
						}
					}, {
						"parameter": {
							"name": "LANGUAGE",
							"value": "[LANGUAGE]",
							"format": "",
							"name_lang": {
								"br": "",
								"de": "",
								"gb": "",
								"es": "",
								"fr": "",
								"it": ""
							}
						}
					}]
				}
			}, {
				"item": {
					"name": "Pré Reserva de Assento",
					"tag": "PreReservaDeAssentoTablet",
					"name_lang": {
						"br": "Pré Reserva de Assento",
						"de": "Sitzplatz-Vorreservierung",
						"gb": "Pre-booking Seats",
						"es": "Reserva previa de asiento",
						"fr": "Pré-réservation de siège",
						"it": "Prenotazione anticipata del posto"
					},
					"service_url": "https://book.latam.com/TAM/dyn/air/servicing/servicingeligibility?SITE=JJBKJJBK&DEVICE_TYPE=DESKTOP&WDS_MARKET=BR&INDENT_OUTPUT=TRUE&DIRECT_RETRIEVE=TRUE",
					"icon": "imgs/icons/RESERVA_ASSENTO.png",
					"api_callback": "freeSeatMap",
					"visible": true,
					"lightbox": true,
					"msgLightbox": {
						"br": "Você pode fazer uma pré-reserva do seu assento no nosso site. É rápido, fácil. Para garantir o assento,  você deve confirmá-lo no momento do Check-in.",
						"de": "Sie können auf unserer Website Sitzplätze vor-reservieren. Das geht schnell und leicht. Um sich den Sitzplatz zu sichern, müssen Sie ihn beim Check-in bestätigen.",
						"gb": "You can pre-book your seat on our website. It's quick and easy. To guarantee your seat, you need to confirm it when you check in.",
						"es": "Puede realizar una reserva previa de su asiento en nuestro sitio web. Es rápido y fácil. Para garantizar el asiento, debe confirmarlo en el momento de hacer el check-in.",
						"fr": "Vous pouvez effectuer une préréservation de votre siège sur notre site. C’est simple et rapide. Pour garantir votre siège, vous devez confirmer au moment de l’enregistrement.",
						"it": "Sul nostro sito può prenotare in anticipo i posti. In modo semplice e rapido. Il posto dovrà essere confermato al momento del check-in."
					},
					"parameters": [{
						"parameter": {
							"name": "DIRECT_RETRIEVE_LASTNAME",
							"value": "",
							"format": "",
							"name_lang": {
								"br": "Sobrenome",
								"de": "Nachname",
								"gb": "Last Name",
								"es": "Apellido",
								"fr": "Nom de famille",
								"it": "Cognome"
							}
						}
					}, {
						"parameter": {
							"name": "REC_LOC",
							"value": "",
							"format": "",
							"name_lang": {






								"br": "Código da reserva",
								"de": "Reservierungscode",
								"gb": "Booking code",
								"es": "Código de reserva",
								"fr": "Code de réservation",
								"it": "Codice di prenotazione"
							}
						}
					}, {
						"parameter": {
							"name": "LANGUAGE",
							"value": "[LANGUAGE]",
							"format": "",
							"name_lang": {
								"br": "",
								"de": "",
								"gb": "",
								"es": "",
								"fr": "",
								"it": ""
							}
						}
					}]
				}
			}, {
				"item": {
					"name": "Alteração de Assento",
					"tag": "AlteracaoDeAssentoTablet",
					"name_lang": {
						"br": "Alteração de Assento",
						"de": "Änderung des Sitzplatzes",
						"gb": "Change your seats",
						"es": "Cambio de asiento",
						"fr": "Modification de siège",
						"it": "Modifica del posto"
					},

					"service_url": "https://book.latam.com/TAM/dyn/air/servicing/servicingeligibility?SITE=JJBKJJBK&DEVICE_TYPE=DESKTOP&WDS_MARKET=BR&INDENT_OUTPUT=TRUE&DIRECT_RETRIEVE=TRUE",
					"icon": "imgs/icons/ASSENTO.png",
					"api_callback": "",
					"visible": true,
					"lightbox": true,
					"msgLightbox": {
						"br": "Mude seu assento",
						"de": "Ändern Sie Ihren Sitzplatz",
						"gb": "Change your seat",
						"es": "Cambie su asiento",
						"fr": "Modifiez votre siège",
						"it": "Modifichi il suo posto"
					},
					"parameters": [{
						"parameter": {
							"name": "DIRECT_RETRIEVE_LASTNAME",
							"value": "",
							"format": "",
							"name_lang": {
								"br": "Sobrenome",
								"de": "Nachname",
								"gb": "Last Name",
								"es": "Apellido",
								"fr": "Nom de famille",
								"it": "Cognome"
							}
						}
					}, {
						"parameter": {
							"name": "REC_LOC",
							"value": "",
							"format": "",
							"name_lang": {






								"br": "Código da reserva",
								"de": "Reservierungscode",
								"gb": "Booking code",
								"es": "Código de reserva",
								"fr": "Code de réservation",
								"it": "Codice di prenotazione"
							}
						}
					}, {
						"parameter": {
							"name": "LANGUAGE",
							"value": "[LANGUAGE]",
							"format": "",
							"name_lang": {
								"br": "",
								"de": "",
								"gb": "",
								"es": "",
								"fr": "",
								"it": ""
							}
						}
					}]
				}
			}]
		}
	}, {
		"item": {
			"name": "Compra Espaço +",
			"tag": "CompraEspacoMaisTablet",
			"name_lang": {
				"br": "Compra Espaço +",
				"de": "Sitzplatz+ erwerben",
				"gb": "Purchase + Space",
				"es": "Comprar Espacio +",
				"fr": "Achat espace +",
				"it": "Acquisto Espaço +"
			},
			"service_url": "https://www.latam.com/b2c/jsp/latam/entryPoint/entryPointAmadeus.jsp?embedded=false&parametro=chargeableSeatMap",
			"api_callback": "",
			"icon": "imgs/icons/RESERVA_ASSENTO.png",
			"visible": true,
			"lightbox": false,
			"msgLightbox": {
				"br": "Viaje com mais conforto! Com Espaço + você compra assentos localizados nas saídas de emergência e na primeira fileira, ideais para quem deseja acomodar melhor as pernas.",
				"de": "Reisen Sie mit mehr Komfort! Mit Sitzplatz+ sichern Sie sich Sitzplätze an den Notausgängen oder in der ersten Reihe. Ideal für Reisende, die sich mehr Beinfreiheit wünschen.",
				"gb": "Travel in more comfort! With Space +, you buy seats located in the emergency exit rows and in the first row, ideal for those who require more leg room.",
				"es": "¡Viaje con mayor comodidad! Con Espacio +, usted compra asientos ubicados en las salidas de emergencia y en la primera hilera; estos asientos son ideales para quienes desean más espacio para las piernas.",
				"fr": "Voyagez plus confortablement ! Avec Space +, vous achetez des sièges situés dans la première rangée et celle des issues de secours. Idéal pour ceux qui ont besoin de plus d’espace pour les jambes.",
				"it": "Viaggi nel massimo del comfort! Con Espaço + può acquistare posti presso le uscite di emergenza e nella prima fila, con più spazio per le gambe."
			},
			"parameters": [{
				"parameter": {
					"name": "LANGUAGE",
					"value": "[LANGUAGE]",
					"format": "",
					"name_lang": {
						"br": "",
						"de": "",
						"gb": "",
						"es": "",
						"fr": "",
						"it": ""
					}
				}
			}, {
				"parameter": {
					"name": "DIRECT_RETRIEVE_LASTNAME",
					"value": "",
					"format": "",
					"name_lang": {
						"br": "Sobrenome",
						"de": "Nachname",
						"gb": "Last Name",
						"es": "Apellido",
						"fr": "Nom de famille",
						"it": "Cognome"
					}
				}
			}, {
				"parameter": {
					"name": "REC_LOC",
					"value": "",
					"format": "",
					"name_lang": {
						"br": "Código da reserva",
						"de": "Reservierungscode",
						"gb": "Booking code",
						"es": "Código de reserva",
						"fr": "Code de réservation",

						"it": "Codice di prenotazione"
					}
				}
			}]
		}
	}, {
		"item": {
			"name": "Status do Voo",
			"tag": "StatusDoVooTablet",
			"name_lang": {
				"br": "Status do Voo",
				"de": "Status des Fluges",
				"gb": "Flight Status",
				"es": "Estado del vuelo",
				"fr": "Statut du vol",
				"it": "Stato del volo"
			},
			"service_url": "https://www.latam.com/pt_br/apps/pessoas/flightstatus?utm_source=AGTVirtual&utm_medium=Julia&utm_content=&utm_campaign=JuliaConteudo_AMS_BR_PT_jun13indef",
			"icon": "imgs/icons/YOUR-NEXT-FLIGHT.png",
			"api_callback": "",
			"visible": true,
			"lightbox": false
		}
	}, {
		"item": {
			"name": "Reembolso de bilhete",
			"tag": "ReembolsoDeBilheteTablet",
			"name_lang": {
				"br": "Reembolso de bilhete",
				"de": "Flugticket erstatten",
				"gb": "Ticket reimbursement",
				"es": "Reembolso del pasaje",
				"fr": "Remboursement du billet",
				"it": "Rimborso del biglietto"
			},
			"service_url": "https://www.latam.com/pt_br/administre-sua-reserva-e-inclua-servicos/reembolso-de-passagem/como-fazer-o-reembolso/",
			"icon": "imgs/icons/APP.png",
			"api_callback": "",
			"visible": true,
			"lightbox": false
		}
	}, {
		"item": {
			"name": "LATAM Fidelidade",
			"tag": "LATAMFidelidadeTablet",
			"name_lang": {
				"br": "LATAM Fidelidade",
				"de": "Treueprogramm LATAM Fidelidade",
				"gb": "LATAM loyalty",
				"es": "LATAM Fidelidade",
				"fr": "LATAM Fidelidade",
				"it": "LATAM Fidelidade"
			},
			"service_url": "",
			"api_callback": "",
			"icon": "imgs/icons/IDENTIFICATION-CARD.png",
			"visible": true,
			"lightbox": false,
			"subitens": [{
				"item": {
					"name": "O programa",
					"tag": "oProgramaFidelidadeTablet",
					"name_lang": {
						"br": "O programa",
						"de": "Das Programm",
						"gb": "The Program",
						"es": "El programa",
						"fr": "Le Programme",
						"it": "Il programma"
					},
					"service_url": "https://www.latam.com/pt_br/latam-fidelidade/nosso-programa/descubra-latam-fidelidade/conheca-o-programa/",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/latam-fidelidade/nosso-programa/descubra-latam-fidelidade/conheca-o-programa/",
						"de": "https://www.latam.com/de_de/",
						"gb": "https://www.latam.com/en_un/",
						"es": "https://www.latam.com/es_us/",
						"fr": "https://www.latam.com/fr_fr/",
						"it": "https://www.latam.com/it_it/"
					},
					"icon": "imgs/icons/IDENTIFICATION-CARD.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Cadastre-se",
					"tag": "CadastraseFidelidadeTablet",
					"name_lang": {
						"br": "Cadastre-se",
						"de": "Registrieren",
						"gb": "Register",
						"es": "Regístrese",
						"fr": "S’inscrire",
						"it": "Registrati"
					},
					"service_url": "https://www.pontosmultiplus.com.br/cadastro/dados/?s_cid=cdst-parceiro-cadastre_se-0-menu_tam_fid-tam-redirect-0-cadastro-0-fullpontos&utm_source=cdst_parceiro&utm_medium=redirect&utm_campaign=cadastro-fullpontos&utm_term=menu_tam_fid_cadastre_se-0-tam-0-0-fullpontos",
					"icon": "imgs/icons/FORM.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Extrato",
					"tag": "ExtratoTablet",
					"name_lang": {
						"br": "Extrato",
						"de": "Extrakt",
						"gb": "Statement",
						"es": "Extracto",
						"fr": "Extrait",
						"it": "Estratto"
					},
					"service_url": "https://www.latam.com/cgi-bin/site_login.cgi",
					"icon": "imgs/icons/MOBILE-CHECKIN.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Como acumular pontos",
					"tag": "ComoAcumularPontosTablet",
					"name_lang": {
						"br": "Como acumular pontos",
						"de": "Wie man Punkte sammeln kann",
						"gb": "How to Accumulate Points",
						"es": "Cómo acumular puntos",
						"fr": "Comment accumuler des points",
						"it": "Come accumulare punti"
					},
					"service_url": "https://www.latam.com/pt_br/latam-fidelidade/como-ganhar-mais-pontos/pontos-em-voos/voos-latam-nacional-e-internacional/",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/latam-fidelidade/como-ganhar-mais-pontos/pontos-em-voos/voos-latam-nacional-e-internacional/",
						"de": "https://www.latam.com/de_de/",
						"gb": "https://www.latam.com/en_un/",
						"es": "https://www.latam.com/es_us/",
						"fr": "https://www.latam.com/fr_fr/",
						"it": "https://www.latam.com/it_it/"
					},
					"icon": "imgs/icons/MOBILE-PHONE.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}]
		}
	}, {
		"item": {
			"name": "Outras Informações",
			"tag": "OutrasInformacoesTablet",
			"name_lang": {
				"br": "Outras Informações",
				"de": "Andere Informationen",
				"gb": "Transportation between airports",
				"es": "Otra información",
				"fr": "Informations supplémentaires",
				"it": "Altre informazioni"
			},
			"service_url": "",
			"api_callback": "",
			"icon": "imgs/icons/bgOthersInfo.png",
			"visible": true,
			"lightbox": false,
			"subitens": [{
				"item": {
					"name": "Informações sobre Bagagem",
					"tag": "InformacoesSobreBagagemTablet",
					"name_lang": {
						"br": "Informações sobre Bagagem",
						"de": "Informationen zum Gepäck",
						"gb": "Baggage Information",
						"es": "Información sobre el equipaje",
						"fr": "Informations sur les bagages",
						"it": "Informazioni sui bagagli"
					},
					"service_url": "https://www.latam.com/pt_br/informacao-para-sua-viagem/?utm_source=AGTVirtual&utm_medium=Julia&utm_content=&utm_campaign=JuliaConteudo_AMS_BR_PT_jun13indef",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/informacao-para-sua-viagem/",
						"de": "https://www.latam.com/de_de/",
						"gb": "https://www.latam.com/en_un/",
						"es": "https://www.latam.com/es_us/",
						"fr": "https://www.latam.com/fr_fr/",
						"it": "https://www.latam.com/it_it/"
					},
					"icon": "imgs/icons/BAGGAGE-DROP.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Companhias aéreas parceiras",
					"tag": "CompanhiasAereasParceiras",
					"name_lang": {
						"br": "Companhias aéreas parceiras",
						"de": "Partner Fluggesellschaften",
						"gb": "Partner airlines",
						"es": "Aerolíneas asociadas",
						"fr": "Compagnies aériennes partenaires",
						"it": "Compagnie aeree partner"
					},
					"service_url": "https://www.latam.com/pt_br/latam-fidelidade/como-ganhar-mais-pontos/pontos-em-voos/voos-aliancas-nacional-e-internacional/",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/latam-fidelidade/como-ganhar-mais-pontos/pontos-em-voos/voos-aliancas-nacional-e-internacional/",
						"de": "https://www.latam.com/de_de/",
						"gb": "https://www.latam.com/en_un/",
						"es": "https://www.latam.com/es_us/",
						"fr": "https://www.latam.com/fr_fr/",
						"it": "https://www.latam.com/it_it/"
					},
					"icon": "imgs/icons/FLIGHT4.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Documentação para Embarque",
					"tag": "DocumentacaoParaEmbarqueTablet",
					"name_lang": {
						"br": "Documentação para Embarque",
						"de": "Wichtige Dokumente für den Flug",
						"gb": "Documentation for boarding",
						"es": "Documentación para el embarque",
						"fr": "Papiers pour embarquement",
						"it": "Documentazione per l’imbarco"
					},
					"service_url": "https://www.latam.com/pt_br/informacao-para-sua-viagem/documentacao-e-vacinas/documentacao-para-embarque/nacional/?utm_source=AGTVirtual&utm_medium=Julia&utm_content=&utm_campaign=JuliaConteudo_AMS_BR_PT_jun13indef",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/informacao-para-sua-viagem/documentacao-e-vacinas/documentacao-para-embarque/nacional/",
						"de": "https://www.latam.com/de_de/",
						"gb": "https://www.latam.com/en_un/",
						"es": "https://www.latam.com/es_us/",
						"fr": "https://www.latam.com/fr_fr/",
						"it": "https://www.latam.com/it_it/"
					},
					"icon": "imgs/icons/DOCUMENTS3.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Embarque de menores",
					"tag": "EmbarqueDeMenoresTablet",
					"name_lang": {
						"br": "Embarque de menores",
						"de": "Boarding von Minderjährigen",
						"gb": "Boarding of Minors",
						"es": "Embarque de menores",
						"fr": "Embarquement des mineurs",
						"it": "Imbarco di minori"
					},
					"service_url": "https://www.latam.com/pt_br/informacao-para-sua-viagem/voando-com-criancas-e-bebes/bebes-e-criancas-a-bordo/?utm_source=AGTVirtual&utm_medium=Julia&utm_content=&utm_campaign=JuliaConteudo_AMS_BR_PT_jun13indef",
				    "url_idioma": {
						"br": "https://www.latam.com/pt_br/informacao-para-sua-viagem/voando-com-criancas-e-bebes/bebes-e-criancas-a-bordo/",
						"de": "https://www.latam.com/de_de/",
						"gb": "https://www.latam.com/en_un/",
						"es": "https://www.latam.com/es_us/",
						"fr": "https://www.latam.com/fr_fr/",
						"it": "https://www.latam.com/it_it/"
					},
					"icon": "imgs/icons/ESPECIAL.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Entretenimento",
					"tag": "EntretenimentoTablet",
					"name_lang": {
						"br": "Entretenimento",
						"de": "Unterhaltung",
						"gb": "Entertainment",
						"es": "Entretenimiento",
						"fr": "Divertissement",
						"it": "Intrattenimento"
					},
					"service_url": "https://www.latam.com/pt_br/conheca-nos/experiencia-a-bordo/entretenimento/?utm_source=AGTVirtual&utm_medium=Julia&utm_content=&utm_campaign=JuliaConteudo_AMS_BR_PT_jun13indef",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/conheca-nos/experiencia-a-bordo/entretenimento/",
					    "de": "https://www.latam.com/de_de/",
						"gb": "https://www.latam.com/en_un/",
						"es": "https://www.latam.com/es_us/",
						"fr": "https://www.latam.com/fr_fr/",
						"it": "https://www.latam.com/it_it/"
					},
					"icon": "imgs/icons/EARPHONE-HEADPHONE.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Necessidades especiais",
					"tag": "NecessidadesEspeciaisTablet",
					"name_lang": {
						"br": "Necessidades especiais",
						"de": "Besondere Bedürfnisse ",
						"gb": "Special needs",
						"es": "Necesidades especiales",
						"fr": "Besoins spécifiques",
						"it": "Special necessities"
					},
					"service_url": "https://www.latam.com/pt_br/informacao-para-sua-viagem/necessidades-especiais/passageiros-com-necessidades-medicas/?utm_source=AGTVirtual&utm_medium=Julia&utm_content=&utm_campaign=JuliaConteudo_AMS_BR_PT_jun13indef",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/informacao-para-sua-viagem/necessidades-especiais/passageiros-com-necessidades-medicas/",
						"de": "https://www.latam.com/de_de/reiseinformationen/besondere-hilfeleistungen/reisende-mit-besonderen-bedurfnissen/",
						"gb": "https://www.latam.com/en_au/travel-information/special-assistance/special-needs/",
						"es": "https://www.latam.com/es_es/informacion-para-tu-viaje/necesidades-especiales/pasajeros-con-necesidades-especiales/",
						"fr": "https://www.latam.com/fr_fr/informations-de-voyage/assistance-speciale/passagers-aux-besoins-speciaux/",
						"it": "https://www.latam.com/it_it/informazioni-per-il-tuo-viaggio/necessita-particolari/passeggeri-con-necessita-particolari/"
					},
					"icon": "imgs/icons/ESPECIAL2.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Sala Vip",
					"tag": "SalaVipTablet",
					"name_lang": {
						"br": "Sala Vip",
						"de": "Vip-Lounge",
						"gb": "Vip Room",
						"es": "Sala Vip",
						"fr": "Salon VIP",
						"it": "Sala Vip"
					},
					"service_url": "https://www.latam.com/pt_br/conheca-nos/salas-vip/salas-vip-latam/?utm_source=AGTVirtual&utm_medium=Julia&utm_content=&utm_campaign=JuliaConteudo_AMS_BR_PT_jun13indef",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/conheca-nos/salas-vip/salas-vip-latam/",
						"de": "https://www.latam.com/de_de/uber-uns/vip-lounges/vip-lounges-/",
						"gb": "https://www.latam.com/en_ue/about-us/vip-lounges/vip-lounges-latam/",
						"es": "https://www.latam.com/es_es/conocenos/salones-vip/salones-vip/",
						"fr": "https://www.latam.com/fr_fr/a-propos-de-nous/salons-vip/salons-vip-latam/",
						"it": "https://www.latam.com/it_it/conoscerci/salones-vip/sale-vip/"
					},
					"icon": "imgs/icons/DRINKS.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Transporte entre aeroportos",
					"tag": "TransporteEntreAeroportosTablet",
					"name_lang": {
						"br": "Transporte entre aeroportos",
						"de": "Transport zwischen Flughäfen",
						"gb": "Transportation between airports",
						"es": "Transporte entre aeropuertos",
						"fr": "Transport entre les aéroports",
						"it": "Trasporto tra gli aeroporti"
					},
					"service_url": "https://www.latam.com/pt_br/informacao-para-sua-viagem/onibus-para-aeroportos/?utm_source=AGTVirtual&utm_medium=Julia&utm_content=&utm_campaign=JuliaConteudo_AMS_BR_PT_jun13indef",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/informacao-para-sua-viagem/onibus-para-aeroportos/",
						"de": "https://www.latam.com/de_de/",
						"gb": "https://www.latam.com/en_un/",
						"es": "https://www.latam.com/es_us/",
						"fr": "https://www.latam.com/fr_fr/",
						"it": "https://www.latam.com/it_it/"
					},
					"icon": "imgs/icons/TRANSPORT.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Transporte de Animais",
					"tag": "TransporteDeAnimaisTablet",
					"name_lang": {
						"br": "Transporte de Animais",
						"de": "Reisen mit Haustieren",
						"gb": "Transportation of Animals",
						"es": "Transporte de animales",
						"fr": "Transport des animaux",
						"it": "Trasporto di animali"
					},
					"service_url": "https://www.latam.com/pt_br/informacao-para-sua-viagem/como-viajar-com-cachorro-e-gato/?utm_source=AGTVirtual&utm_medium=Julia&utm_content=&utm_campaign=JuliaConteudo_AMS_BR_PT_jun13indef",
					"url_idioma": {

						"br": "https://www.latam.com/pt_br/informacao-para-sua-viagem/como-viajar-com-cachorro-e-gato/",
						"de": "https://www.latam.com/de_de/reiseinformationen/reisen-mit-ihrem-haustier/",



						"gb": "https://www.latam.com/en_ue/travel-information/traveling-with-pets/",
						"es": "https://www.latam.com/es_es/informacion-para-tu-viaje/como-viajar-con-tu-mascota/",
						"fr": "https://www.latam.com/fr_fr/informations-de-voyage/voyagez-avec-votre-animal/",
						"it": "https://www.latam.com/it_it/informazioni-per-il-tuo-viaggio/viaggia-insieme-al-tuo-animale-da-compagnia/"
					},
					"icon": "imgs/icons/ANIMALS.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}, {
				"item": {
					"name": "Viajando pela primeira vez",
					"tag": "ViajandoPelaPrimeiraVezTablet",
					"name_lang": {
						"br": "Viajando pela primeira vez",
						"de": "Die erste Reise",
						"gb": "Traveling for the first time",
						"es": "Viajando por primera vez",
						"fr": "Voyager pour la première fois",
						"it": "Viaggiare per la prima volta"
					},
					"service_url": "https://www.latam.com/pt_br/informacao-para-sua-viagem/primera-viagem/como-comprar-sua-passagem/",
					"url_idioma": {
						"br": "https://www.latam.com/pt_br/informacao-para-sua-viagem/primera-viagem/como-comprar-sua-passagem/",
						"de": "https://www.latam.com/de_de/",
						"gb": "https://www.latam.com/en_un/",
						"es": "https://www.latam.com/es_us/",
						"fr": "https://www.latam.com/fr_fr/",
						"it": "https://www.latam.com/it_it/"
					},
					"icon": "imgs/icons/DESTINATION.png",
					"api_callback": "",
					"visible": true,
					"lightbox": false
				}
			}]
		}
	}]
}